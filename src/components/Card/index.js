import React from 'react';
import './styles.css';

export default function Card({ className, children }) {
  return <div className={`card ${className}`}>{children}</div>;
}
