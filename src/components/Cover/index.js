import React from 'react';
import './styles.css';

export default function Cover({ cover }) {
  return (
    <>
      <div className="cover">
        <img className="image" src={cover} alt="Capa" />
      </div>
    </>
  );
}
