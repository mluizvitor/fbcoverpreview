import React from 'react';
import './styles.css';

export default function Feed({ children }) {
  return <div className="feed">{children}</div>;
}
